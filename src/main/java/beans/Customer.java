package beans;

public interface Customer extends Comparable<Customer> {


    Double costOfItemsPurchased();

}
