# Setup
Pretty standard Maven project using vanilla Java 11. No frameworks or dependencies required. 

You will need to bring your own Java JDK - I am using the JetBrains JDK 11 but any 11 compatible should work

To run from the terminal/command prompt you can use the bundled Maven Wrapper doing a one of these:

`./mvnw clean compile exec:java`

or for Windows folks

`.\mvnw.cmd clean compile exec:java`

# The Exercise
1. In the `StoreFrontCustomer` class, do the following
    *  Create an all-args constructor
    *  implement the `Customer` interface, its inherited methods, and a `toString`
        *  `costOfItemsPurchased()` - returns the number of items purchased multiplied by the price of the item
        *  `compareTo(Customer customer)` - comparable method that uses costOfItemsPurchased() for comparison
        *  `toString()` - returns a string representation of the class using the following as a template
            *  _"[customerName] is purchasing [quantityPurchased] [itemName] for [itemPrice] each, [costOfItemsPurchased()] total"_
2. in the `StoreFront` class write a `parseStoreFrontCustomer(String... customerData)` method that
    *  parse/convert/otherwise the `customerData` `String[]` into appropriate values
    *  instantiates and populates a `StoreFrontCustomer` with the values
    *  return the `StoreFrontCustomer` to the caller.
3. in the `StoreFront` write a `operateStoreFront(String[][] customerData)` method that
    *  builds a `Collection` or an `array` of `StoreFrontCustomer` objects using the `parseStoreFrontCustomer()` method.
    *  sort the `storeFrontCustomers` by `costOfItemsPurchased()`
    *  print the `storeFrontCustomers` with `System.out.println()`
4. Bonus 1
    *  reverse the `storeFrontCustomers` `Collection`/`Array` and print each `StoreFrontCustomer` with `System.out.println()`
5. Bonus 2
    *  print each `StoreFrontCustomer` the `storeFrontCustomers` `Collection`/`Array` with `System.out.println()`, but reverse the `toString()`